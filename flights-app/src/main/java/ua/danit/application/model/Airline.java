package ua.danit.application.model;

public class Airline {
  /**

   Airline ID	Unique OpenFlights identifier for this airline.
   Name	Name of the airline.
   Alias	Alias of the airline. For example, All Nippon Airways is commonly known as "ANA".
   IATA	2-letter IATA code, if available.
   ICAO	3-letter ICAO code, if available.
   Callsign	Airline callsign.
   Country	Country or territory where airline is incorporated.
   Active	"Y" if the airline is or has until recently been operational, "N" if it is defunct. This field is not reliable: in particular, major airlines that stopped flying long ago, but have not had their IATA code reassigned (eg. Ansett/AN), will incorrectly show as "Y".
   The data is ISO 8859-1 (Latin-1) encoded. The special value \N is used for "NULL" to indicate that no value is available, and is understood automatically by MySQL if imported.

   Notes: Airlines with null codes/callsigns/countries generally represent user-added airlines. Since the data is intended primarily for current flights, defunct IATA codes are generally not included. For example, "Sabena" is not listed with a SN IATA code, since "SN" is presently used by its successor Brussels Airlines.

   Sample entries

   324,"All Nippon Airways","ANA All Nippon Airways","NH","ANA","ALL NIPPON","Japan","Y"
   412,"Aerolineas Argentinas",\N,"AR","ARG","ARGENTINA","Argentina","Y"
   413,"Arrowhead Airways",\N,"","ARH","ARROWHEAD","United States","N"
   */
  public Airline(){
  }

  public Airline(int airlineID, String name, String alias, String iATA, String iCAO, String callsign, String country, char active) {
      this.airlineID = airlineID;
      this.name = name;
      this.alias = alias;
      this.iATA = iATA;
      this.iCAO = iCAO;
      this.callsign = callsign;
      this.country = country;
      this.active = active;
  }

  private long airlineID;

  private String name;

  private String alias;

  private String iATA;

  private String iCAO;

  private String callsign;

  private String country;

  private char active;

    public long getAirlineID() {
        return airlineID;
    }

    public String getName() {
        return name;
    }

    public String getAlias() {
        return alias;
    }

    public String getiATA() {
        return iATA;
    }

    public String getiCAO() {
        return iCAO;
    }

    public String getCallsign() {
        return callsign;
    }

    public String getCountry() {
        return country;
    }

    public char getActive() {
        return active;
    }




}
